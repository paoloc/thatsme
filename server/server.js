 var express = require('express');
 var app = express();
 var path = require('path');


 app.use(require('connect-livereload')({
     port: 7777
 }));



var patchClient=path.join(__dirname, '..', 'client');
// serve static files
app.use(express.static(patchClient));

 
 // catch 404
 app.get('/:url(api|bower_components|assets)/*', function(req, res) {
     res.status(404).send('Resource not found');
 });

 // serve always index.html
 app.get('/*', function(req, res) {
     res.sendFile(path.join(__dirname, '..', 'client', 'index.html'));
 });

 // Start server
 app.listen(8080, function() {
     console.log('Express server listening on %d', 8080);
 });
