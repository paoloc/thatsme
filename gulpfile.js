var gulp = require('gulp');
var server = require('gulp-express');
var env = require('gulp-env');


gulp.task('default', function() {
  // place code for your default task here
  // Start the server at the beginning of the task 
  server.run(['server/server.js'], {}, 7777);

  // notify the client
  gulp.watch(['client/app/**/*.html'], server.notify);
  gulp.watch(['client/assets/**/*'], server.notify);
  gulp.watch(['client/app/**/*.js'], server.notify);

  // Restart the server when file changes 
  gulp.watch(['server/**/*.js'], [server.run]);

});
